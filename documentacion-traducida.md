# Webpack

## Conceptos:

En su centro, __Webpack es un static module bundler(empaquetador de modulos estaticos) para aplicaciones hechas en javascript moderno__. Cuando Webpack procesa tu aplicacion, esta internamente crea un dependency graph (grafico de dependencias) que mapea cada módulo que tu proyecto necesite y generar uno o más paquetes.

Grafico de dependencias (Dependency Graph): Cada vez que un archivo depende de otro, webpack lo trata como una dependencia. Esto permite que webpack para tomar non-code assets, como imágenes o web fonts, y también los proporcione como dependencias para su aplicación.
Cuando webpack procesa su aplicación, este se inicia desde una lista de módulos definidos en la línea de comandos o en su archivo de configuración. A partir de estos entry-points(concepto de webpack), webpack crea de forma recursiva un gráfico de dependencia que incluye cada módulo que su aplicación necesita, y luego agrupa todos esos módulos en una pequeña cantidad de paquetes, a menudo, solo uno, que debe cargar el navegador.


Desde la version 4.0.0, webpack no requiere de un fichero de configuracion para empaquetar tu proyecto, sin embargo este es increiblemente configurable para adapatarse mejor a tus necesidades.

Para una mejor comprensión de las ideas detrás de los paquetes de módulos y cómo funcionan internamente, consulte estos recursos:

- [Agrupar manualmente una aplicación | Manually Bundling an Application](https://www.youtube.com/watch?v=UNMkLHzofQI)

- [Codigo en vivo de un empaquedor de modulos simples | Live Coding a Simple Module Bundler](https://www.youtube.com/watch?v=Gc9-7PBqOC8)

- [Explicacion detallada de un empaquetador de modulos simples | Detailed Explanation of a Simple Module Bundler](https://github.com/ronami/minipack)

Para comenzar solo necesitas entender sus conceptos básicos:

- [Entry (entrada/punto de entrada)](Entry)
- [Output (salida/punto de salida)](Entry)
- [Loaders (Cargadores)](Entry)
- [Plugins](Entry)
- [Mode (modo)](Entry)
- [Browser Compatibility (compatibilidad de navegadores)](Entry)

El objetivo de este documento es ofrecer una descripción general de alto nivel de estos conceptos, al tiempo que proporciona enlaces a casos de uso detallados y específicos del concepto.

### Entry :

